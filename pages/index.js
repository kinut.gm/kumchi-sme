import {
  Tabs,
  Badge,
  Avatar,
  Button,
  Tag,
  Modal,
  Input,
  InputNumber,
  Popconfirm,
  DatePicker,
  Statistic,
  Select,
  Steps,
  Divider,
} from "antd";
import { FaFilter, FaMinus, FaPlus, FaSearch, FaUserAlt } from "react-icons/fa";
import { BiKey, BiUser } from "react-icons/bi";
import { useState } from "react";
import Image from "next/image";

const { Option } = Select;
const { Step } = Steps;
const { RangePicker } = DatePicker;

const Dashboard = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [password2, setPassword2] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [name, setName] = useState(null);

  const handleLogOut = () => {
    setLoggedIn(false);
  };

  const Login = () => {
    const handleLogin = () => {
      setLoggedIn(true);
    };
    const handleSignup = () => {
      setLoggedIn(true);
    };

    return (
      <div className="h-screen w-screen bg-blue-500 relative">
        <div className="p-4 absolute top-[30%] left-[50%] translate-x-[-50%] rounded-md bg-white sm:max-w-[70%] md:max-w-[40%]">
          <h2 className="text-xl text-center tracking-tighter uppercase">
            Suppliers Portal
          </h2>
          <Divider />
          <Input
            placeholder="Username / Phone number"
            prefix={<BiUser className="text-gray-500" />}
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <Input.Password
            className="my-3"
            placeholder="Password"
            prefix={<BiKey className="text-gray-500" />}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button type="primary" block onClick={handleLogin}>
            Continue
          </Button>
          <p className="text-center m-0 mt-3">
            Not a supplier yet?{" "}
            <Button
              type="link"
              style={{ display: "inline" }}
              onClick={() => setModalVisible(true)}
            >
              Become a supplier
            </Button>
          </p>
          <Modal
            open={modalVisible}
            title="Be a supplier!"
            onOk={handleSignup}
            onCancel={() => setModalVisible(false)}
          >
            <p>
              We noticed you aren&apos;t a supplier just yet. Fill your business
              details down below to get you started{" "}
            </p>
            <Input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name ex. Kamwangi suppliers"
            />
            <Input
              value={phoneNumber}
              onChange={(e) => setPhoneNumber(e.target.value)}
              placeholder="Phone number ex. 0748920306"
              style={{ marginTop: "12px" }}
            />
            <Input.Password
              className="my-3"
              placeholder="Password"
              prefix={<BiKey className="text-gray-500" />}
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
            />
          </Modal>
        </div>
      </div>
    );
  };

  const SupplyRequest = () => {
    const handleSell = () => {};
    return (
      <div className="p-2 bg-gray-50 w-full flex my-1">
        <div className="mr-4">
          <Image width={48} height={48} src={"/favicon.ico"} />
        </div>
        <div className="w-[70%]">
          <p className="font-bold m-0">
            Product name {"  "}
            <Tag color="blue" className="ml-6">
              * 30
            </Tag>
          </p>
          <div className="flex justify-between mt-1">
            <p className="text-blue-500">Shop name</p>
            <p className="text-blue-500">0748930306</p>
          </div>
          <p className="m-0 mt-1">Supply due date: </p>
          <p className="mt-1">
            [About the order] Lorem ipsum dolor sier amor aqua number Lorem
            ipsum dolor sier amor aqua number Lorem ipsum dolor sier amor aqua
            number Lorem ipsum dolor sier amor aqua number Lorem ipsum dolor
            sier amor aqua number Lorem ipsum dolor sier amor aqua number Lorem
            ipsum dolor sier amor aqua number
          </p>
          <Popconfirm title="Record sale?" onConfirm={handleSell}>
            <Button type="primary" size="small" style={{ float: "right" }}>
              Sold
            </Button>
          </Popconfirm>
        </div>
      </div>
    );
  };

  const SupplyRequests = () => {
    const [keyword, setKeyword] = useState(null);
    const [filter, setFilter] = useState("PENDING");

    return (
      <div>
        <div className="flex justify-between mb-2">
          <Input
            suffix={<FaSearch className="text-gray-300" />}
            placeholder="Search order..."
            className="max-w-[400px]"
            value={keyword}
            onChange={(e) => setKeyword(e.target.value)}
          />
          <Select
            suffixIcon={<FaFilter />}
            value={filter}
            className="min-w-[100px]"
            onChange={(val) => setFilter(val)}
          >
            <Option value={"ALL"}>ALL</Option>
            <Option value={"PENDING"}>PENDING</Option>
            <Option value={"ACTUALIZED"}>ACTUALIZED</Option>
          </Select>
        </div>
        <div className="max-h-[calc(100vh-200px)] overflow-y-auto">
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el, i) => (
            <SupplyRequest key={i} />
          ))}
        </div>
      </div>
    );
  };

  const Product = (props) => {
    const [restockModal, setRestockModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [newName, setNewName] = useState(null);
    const [newPrice, setNewPrice] = useState(null);

    const handleEdit = () => {};
    const handleRemove = () => {};
    const incrementStock = () => {};
    const decrementStock = () => {};

    const handleCancel = () => {
      setEditModal(false);
      setRestockModal(false);
      setNewPrice(null);
      setNewName(null);
    };

    return (
      <div className="bg-gray-50 col-span-1 p-3">
        <Image width={100} height={100} src={"/favicon.ico"} />
        <p className="font-bold tracking-tight m-0 mt-2">Product name</p>
        <p className="m-0">Ksh. 200</p>
        <Tag color="orange">23 left</Tag>
        <div className="flex -space-x-2 mt-2">
          <Button type="link" onClick={() => setRestockModal(true)}>
            Restock
          </Button>
          <Button type="link" onClick={() => setEditModal(true)}>
            Edit
          </Button>
          <Popconfirm
            title="Remove product ?"
            okText="Yes"
            cancelText="No"
            onConfirm={handleRemove}
          >
            <Button type="link">Remove</Button>
          </Popconfirm>
        </div>

        <Modal
          visible={restockModal}
          title="Restock"
          footer={null}
          onCancel={handleCancel}
        >
          <p>Update quantity of items:</p>
          <div className="flex space-x-4">
            <Button type="primary" onClick={decrementStock}>
              <FaMinus />
            </Button>
            <h2 className="font-bold">{12}</h2>
            <Button type="primary" onClick={incrementStock}>
              <FaPlus />
            </Button>
          </div>
        </Modal>
        <Modal
          visible={editModal}
          title="Edit"
          onOk={handleEdit}
          onCancel={handleCancel}
        >
          <p>Update product information</p>
          <Input
            value={newName}
            allowClear
            onChange={(e) => setNewName(e.target.value)}
            placeholder="New product name"
          />
          <div className="block w-full my-2">
            <InputNumber
              prefix="Ksh."
              value={newPrice}
              style={{ width: "100%" }}
              allowClear
              onChange={(val) => setNewPrice(val)}
              placeholder="New product price"
            />
          </div>
        </Modal>
      </div>
    );
    ``;
  };

  const ProductList = () => {
    const [addModal, setAddModal] = useState(false);
    const [name, setName] = useState(null);
    const [price, setPrice] = useState(null);
    const [quantity, setQuantity] = useState(null);

    const handleCloseAdd = () => {
      setAddModal(false);
    };

    const handleAddProduct = () => {
      null;
    };

    return (
      <div className="relative">
        <div className="min-h-[calc(100vh-150px)] max-h-[calc(100vh-150px)] overflow-auto grid grid-cols-2 gap-3">
          {[1, 2, 3, 4, 5].map((el, i) => (
            <Product key={i} />
          ))}
        </div>
        <button
          onClick={() => setAddModal(true)}
          className="bg-[#f56a00]-900 p-4 rounded-[50%] border border-gray-300 absolute right-4 bottom-4 "
        >
          <FaPlus />
        </button>
        <Modal
          visible={addModal}
          title="Add product"
          onCancel={handleCloseAdd}
          onOk={handleAddProduct}
        >
          <Input
            value={name}
            required
            onChange={(e) => setName(e.target.value)}
            placeholder="Product name"
          />
          <div className="block w-full my-2">
            <InputNumber
              prefix="Ksh."
              value={price}
              style={{ width: "100%" }}
              required
              onChange={(val) => setPrice(val)}
              placeholder="Price"
            />
          </div>
          <InputNumber
            suffix="Items"
            value={quantity}
            style={{ width: "100%" }}
            required
            onChange={(val) => setQuantity(val)}
            placeholder="Quantity"
          />
        </Modal>
      </div>
    );
  };

  const Sale = (props) => {
    return (
      <div className=" flex  my-1 bg-gray-50 p-3 justify-between">
        <div className="flex space-x-6">
          <div>
            <Image src={"/favicon.ico"} width={48} height={48} />
          </div>
          <div>
            <div className="flex space-x-4 m-0">
              <p className="font-bold m-0">Product name</p>{" "}
              <p classname="text-gray-500 font-light">*</p>{" "}
              <Tag color="blue" style={{ height: 20 }}>
                3
              </Tag>
            </div>
            {props?.sale && (
              <div className="flex space-x-8">
                <p className="text-blue-500">Shop name</p>
                <p className="text-blue-500">0748930306</p>
              </div>
            )}
            <p className="m-0">Ordered: </p>
            <p className="m-0">Delivered: </p>
          </div>
        </div>
        <div>
          <div className="flex">
            <span className="text-sm text-gray-300">Ksh.</span>
            <h2 className="text-xl ml-2">2,000</h2>
          </div>
        </div>
      </div>
    );
  };

  const Vendor = (props) => {
    const [modal, setModal] = useState(false);

    return (
      <div className="bg-gray-50 p-2 my-1 flex justify-between items-center ">
        <div className="flex space-x-6 items-center">
          <Image src={"/favicon.ico"} width={56} height={56} />
          <div>
            <p className="font-bold tracking-tight m-0 ">Shop name</p>
            <Tag color="green">23 total sales</Tag>
          </div>
        </div>
        <Button type="link" onClick={() => setModal(true)}>
          View sales
        </Button>
        <Modal
          open={modal}
          title="Sale history"
          footer={null}
          onCancel={() => setModal(false)}
        >
          <RangePicker
            allowClear
            className="w-full"
            showTime={false}
            placeholder={["From", "To"]}
            onOk={(a, b) => console.log(a)}
          />
          <div className="flex gap-2 my-2">
            <Statistic
              value={200}
              className="border border-gray-100  w-[50%]"
              title="Sale count"
            />
            <Statistic
              className="border border-gray-100 w-[50%]"
              value={30000}
              title="Sales worth "
            />
          </div>
          <div className="max-h-[300px] overflow-y-auto">
            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((el, i) => (
              <Sale key={i} />
            ))}
          </div>
        </Modal>
      </div>
    );
  };

  const VendorList = () => {
    return (
      <div className="p-2 max-h-[calc(100vh-150px)] overflow-y-auto">
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el, i) => (
          <Vendor key={i} />
        ))}
      </div>
    );
  };

  const Sales = () => {
    return (
      <div>
        <RangePicker
          allowClear
          className="w-full"
          showTime={false}
          placeholder={["From", "To"]}
          onOk={(a, b) => console.log(a)}
        />
        <div className="flex gap-2 my-2">
          <Statistic
            value={200}
            className="border border-gray-100  w-[50%]"
            title="Sale count"
          />
          <Statistic
            className="border border-gray-100 w-[50%]"
            value={30000}
            title="Sales worth "
          />
        </div>
        <div className="max-h-[300px] overflow-y-auto">
          {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((el, i) => (
            <Sale sale key={i} />
          ))}
        </div>
      </div>
    );
  };

  const items = [
    {
      label: <Badge dot>Orders</Badge>,
      key: "item-0",
      children: <SupplyRequests />,
    },
    { label: "Products", key: "item-1", children: <ProductList /> },
    { label: "Vendors", key: "item-2", children: <VendorList /> },
    { label: "Sales", key: "item-3", children: <Sales /> },
  ];

  if (!loggedIn) return <Login />;

  return (
    <div>
      {/* Header */}
      <div className="flex justify-between px-2 py-3">
        <p className="font-bold tracking-tight text-gray-900 text-2xl">
          Dashboard
        </p>
        <div className="flex space-x-3">
          <Popconfirm title="Log out" onConfirm={handleLogOut}>
            <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>
              U
            </Avatar>
          </Popconfirm>
        </div>
      </div>

      {/* Tabs */}
      <div className="px-3">
        <Tabs items={items} />
      </div>
    </div>
  );
};

export default Dashboard;
